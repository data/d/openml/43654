# OpenML dataset: Covid-19-ourworldindata

https://www.openml.org/d/43654

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data taken from ourworldindata.org
For more data go here:
Total confirmed cases: https://covid.ourworldindata.org/data/ecdc/total_cases.csv
Total deaths: https://covid.ourworldindata.org/data/ecdc/total_deaths.csv
New confirmed cases: https://covid.ourworldindata.org/data/ecdc/new_cases.csv
New deaths: https://covid.ourworldindata.org/data/ecdc/new_deaths.csv
Full dataset: https://covid.ourworldindata.org/data/ecdc/full_data.csv

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43654) of an [OpenML dataset](https://www.openml.org/d/43654). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43654/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43654/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43654/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

